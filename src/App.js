import FirstView from './components/firstView/firstView';
import HowItWorks from './components/howItWorks/howItWorks';
import AboutPartnership from './components/aboutPartnership/aboutPartnership';
import MobileApplication from './components/mobileApplication/mobileApplication';
import Smartphone from './components/smartphone/smartphone';
import PartnershipIndustry from './components/partnersIndustry/partnersIndustry';
import RequestForm from './components/requestForm/requestForm';
import Contact from './components/contact/contact';

const App = () => {
    return ( 
    <div>
        <FirstView />
        <HowItWorks />
        <AboutPartnership />
        <MobileApplication />
        <Smartphone />
        <PartnershipIndustry />
        <RequestForm />
        <Contact />
    </div> );

}
 
export default App;