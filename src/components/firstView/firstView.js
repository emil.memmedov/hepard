import classes from '././firstView.module.css';

const FirstView = () =>{
    return ( 
    <div className = {classes.FirstView}>
        <div className = {classes.Top}>
            <img src = "./images/icon.png" className = {classes.TopIcon}/>
            <div className = {classes.Info}>
                <div className = {classes.TopContact}>
                    <img src = "./images/phone.png" className = {classes.TopPhoneIcon}/>
                    <p className = {classes.Number}>+994 50 5555555</p>
                </div>
                <div className = {classes.TopLanguage}>
                    <select className = {classes.TopLanguageSelect}>
                        <option className = {classes.TopLanguageOption}>English</option>
                        <option className = {classes.TopLanguageOption}>Azerbaijan</option>
                        <option className = {classes.TopLanguageOption}>Russian</option>
                    </select>
                </div>
                <button className = {classes.TopBtnJoinUs}>Join us</button>
                <a type='button' className = {classes.MobileMenuIcon}><img src = "./images/menuicon.png" /></a>
            </div>
        </div>
        <div className = {classes.About}>
            <h1 className = {classes.AboutSite}>We are Top Courier & Delivery Service in Worldwide</h1>
            <p className = {classes.AboutP}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem 
            <br />Ipsum has been the industry's standard dummy text ever since the 1500s, when an 
            </p>
            <button type = "button" className = {classes.BtnJoinUs}>Join us<img className={classes.ArrowRight} src = "./images/arrow-right.png" /></button>
        </div>
            <img src = "./images/laptop.png" className = {classes.Laptop}/>
            <img src = "./images/laptopMobile.png" className = {classes.LaptopMobile}/>
    </div> );
    
}
 
export default FirstView;