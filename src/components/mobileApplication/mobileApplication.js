import classes from './mobileApplication.module.css';

const MobileApplication = () =>{
    return (
        <div className = {classes.MobileApplication}>
            <div className = {classes.Images}>
                <img className = {classes.ImageBack} src = "./images/phoneBack.png" />
                <img className = {classes.ImageFront} src = "./images/phoneFront.png" />
            </div>
            <div className = {classes.MobImages}>
                <img className = {classes.MobImageBack} src = "./images/appMobBack.png" />
                <img className = {classes.MobImageFront} src = "./images/appMobFront.png" />
            </div>
            <div className = {classes.Writing}>
                <h1 className = {classes.Heading}>It is a long established fact that a reader will be distracted by the </h1>
                <p className = {classes.Paragraph}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make </p>
                <ul className = {classes.List}>
                    <li className = {classes.ListItem}><img className = {classes.ListIcon} src = "./images/checklistIcon.png"/>Integrate your delivery</li>
                    <li className = {classes.ListItem}><img className = {classes.ListIcon} src = "./images/checklistIcon.png"/>Evaluate your customers</li>
                    <li className = {classes.ListItem}><img className = {classes.ListIcon} src = "./images/checklistIcon.png"/>Track your delivery</li>
                    <li className = {classes.ListItem}><img className = {classes.ListIcon} src = "./images/checklistIcon.png"/>Evaluate your customers</li>   
                </ul>
                <button className = {classes.BtnMoreAbout}>Mobile application</button>
            </div>
            
        </div>
    )
}

export default MobileApplication;