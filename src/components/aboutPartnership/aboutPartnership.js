import classes from './aboutPartnership.module.css';

const AboutPartnership = () =>{
    return (
        <div className = {classes.AboutPartnership}>
            <div className = {classes.Images}>
                <img className = {classes.ImageFront} src = "./images/activityFront.png" />
                <img className = {classes.ImageBack} src = "./images/activityBack.png" />
            </div>
            <div className = {classes.MobImages}>
                <img className = {classes.MobImageFront} src = "./images/mobPartnerFront.png" />
                <img className = {classes.MobImageBack} src = "./images/mobPartnerBack.png" />
            </div>
            <h1 className = {classes.Heading}>It is a long established fact that a reader will be distracted by the </h1>
            <p className = {classes.Paragraph}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make </p>
            <ul className = {classes.List}>
                <li className = {classes.ListItem}><img className = {classes.ListIcon} src = "./images/checklistIcon.png"/>Integrate your delivery</li>
                <li className = {classes.ListItem}><img className = {classes.ListIcon} src = "./images/checklistIcon.png"/>Evaluate your customers</li>
                <li className = {classes.ListItem}><img className = {classes.ListIcon} src = "./images/checklistIcon.png"/>Track your delivery</li>
                <li className = {classes.ListItem}><img className = {classes.ListIcon} src = "./images/checklistIcon.png"/>Evaluate your customers</li>   
            </ul>
            <button className = {classes.BtnMoreAbout}>More About Partnership</button>
        </div>
    )
}
export default AboutPartnership;