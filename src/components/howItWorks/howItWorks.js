import React, {Component} from 'react'; 
import classes from "./howItWorks.module.css";
import Section from './sections';

class HowItWorks extends Component {
    state = {
        classList: classes.Header
    }
    componentDidMount = () => {
        document.addEventListener('scroll',()=>{
            if(document.getElementsByClassName(classes.HowItWorks)[0].getBoundingClientRect().top<=0){
                this.setState({classList: classes.Sticky});
            }
            else{
                this.setState({classList: classes.Header});
            }
        })
    }
    render(){
        return(
            <React.Fragment>
                <div className = {this.state.classList}>
                    <img className = {classes.StickyIcon} src = './images/stickyIcon.png' />
                    <div className = {classes.Info}>
                        <div className = {classes.TopContact}>
                            <img src = "./images/stickyPhoneIcon.png" className = {classes.TopPhoneIcon}/>
                            <p className = {classes.Number}>+994 50 5555555</p>
                        </div>
                        <div className = {classes.TopLanguage}>
                            <select className = {classes.TopLanguageSelect}>
                                <option className = {classes.TopLanguageOption}>English</option>
                                <option className = {classes.TopLanguageOption}>Azerbaijan</option>
                                <option className = {classes.TopLanguageOption}>Russian</option>
                            </select>
                        </div>
                        <button className = {classes.TopBtnJoinUs}>Join us</button>
                    </div>
                </div>
                <div className = {classes.HowItWorks}>
                    <div className = {classes.H}>
                        <h1>How it <span className = {classes.Works}> Works</span></h1>
                    </div>
                    <a type = "button" className = {classes.Business}>Business</a>
                    <a type = "button" className = {classes.Delivery}>Delivery</a>
                    <div className = {classes.WorksSections}>
                        <Section 
                        imagePath = "./images/scheduleIcon.png"
                        heading = "Urgent and scheduled"
                        paragraph = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."/>
                        <Section 
                        imagePath = "./images/locationIcon.png"
                        heading = "Urgent and scheduled"
                        paragraph = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."/>
                        <Section 
                        imagePath = "./images/automaticIcon.png"
                        heading = "Urgent and scheduled"
                        paragraph = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."/>
                        <Section 
                        imagePath = "./images/fleetTrackingIcon.png"
                        heading = "Urgent and scheduled"
                        paragraph = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."/>
                    </div>
                </div>
            </React.Fragment>
        )
    }
    
}

export default HowItWorks;