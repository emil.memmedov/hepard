import classes from './sections.module.css';

const Section = (props) =>{
    return(
        <div className = {classes.Section}>
            <img className = {classes.SectionImg} src = {props.imagePath} />
            <h1 className = {classes.Heading}>{props.heading}<br />orders</h1>
            <p className = {classes.Paragraph}>{props.paragraph}</p>
        </div>
    )
}
export default Section;