import classes from './requestForm.module.css';
import React,{Component} from 'react';

class RequestForm extends Component {
    state = {  }
    render() { 
        return ( 
        <div className = {classes.RequestForm}>
            <div className = {classes.Image}>
                <img src = "./images/lastPhone.png" />
            </div>
            <div className = {classes.Form}>
                <h1 className = {classes.Heading}>Request Form</h1>
                <p className = {classes.P}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since </p>
                <form className = {classes.FormEl}>
                    <p className = {classes.Partners}>Partner Type</p>
                    <label for = "bPartner" className = {classes.BusinessPartner}>Business Partner</label>
                    <input id = "bPartner" className = {classes.bPartner} type ="radio" />
                    <label for = "dPartner" className = {classes.DeliveryPartner}>Delivery Partner</label>
                    <input id = "dPartner" className = {classes.dPartner} type ="radio" />
                    <div className = {classes.Fillform}>
                        <label for = "pName" className = {classes.pName}>Person Name</label><input placeholder = "Your Name" type ="text" id = "pName" className = {classes.pNameInp} />
                        <div className = {classes.ch}>
                            <label for = "vehicle" className = {classes.Vehicle}>Vehicle Type</label>
                            <select className = {classes.VType} placeholder= "your vehicle type">
                                <option value = "" disabled selected>Your Vehicle Type</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                            </select>
                        </div>
                        <div className= {classes.ch}>
                            <label for = "city" className = {classes.City}>City</label><input placeholder = "Write your city" type ="text" id = "city" className = {classes.CityInp} />
                        </div>
                        <div className = {classes.ch}>
                            <label for = "phone" className = {classes.Phone}>Phone Number</label><input placeholder = "(994) 55 5555555" type ="text" id = "phone" className = {classes.PhoneInp} />
                        </div>
                        <button type = "submit" className = {classes.SendRequest}>Send Request Form</button>
                    </div>
                    
                </form>
            </div>
        </div> );
    }
}
 
export default RequestForm;