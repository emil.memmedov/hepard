import classes from './contact.module.css';

const Contact = () =>{
    return (
        <div className = {classes.contact}>
            <div className = {classes.SectionOne}>
                <img className = {classes.Icon} src = "/images/stickyIcon.png" />
                <p className = {classes.P}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsumhas been the industry's standard dummy text ever since the 1500s.  </p>
                <div className = {classes.Icons}>
                    <img src = "./images/facebookIcon.png" />
                    <img src = "./images/instagramIcon.png" />
                    <img src = "./images/twitterIcon.png" />
                </div>
            </div>
            <div className = {classes.SectionTwo}>
                <h1 className = {classes.StwoHeading}>Contact</h1>
                <img src = "./images/contactLocationIcon.png" /> <span className = {classes.Location}>H.Əliyev Pr. 125 Bakı</span>
                <br />
                <img src = "./images/mailIcon.png" /> <span className = {classes.Mail}>office@knexel.com</span>
                <br />
                <img src = "./images/contactPhoneIcon.png" /> <span className = {classes.Phone}>+9941233105300</span>
            </div>
            <div className = {classes.SectionThree}>
                <p className = {classes.PApp}>Partner Application</p>
                <button className = {classes.GPlay}><img className={classes.GPlayIcon} src = "./images/gPlay.png" />Google Play</button>
                <button className = {classes.Apple}><img className={classes.AppleIcon} src = "./images/appleIcon.png" />Play Store</button>
            </div>
            <div className = {classes.SectionFour}>
                <p className = {classes.Knexel}>Knexel Technologies © 2020</p>
                <a className = {classes.Terms}>Terms & Conditions</a>
                <a href = "#" className = {classes.Privacy}>Privacy Policy</a>
            </div>
        </div>
    )
}

export default Contact;