import React, {Component} from 'react';
import classes from './partnersIndustry.module.css';
import './style.css';

class PartnershipIndustry extends Component{
    state = {
        images: [
            {imagePath: './images/allIcon.png',name: 'All'},
            {imagePath: './images/foodIcon.png',name: 'Food'},
            {imagePath: './images/medicineIcon.png',name: 'Medicine'},
            {imagePath: './images/parfumIcon.png',name: 'Parfum'},
            {imagePath: './images/clothesIcon.png',name: 'Clothes'},
            {imagePath: './images/giftIcon.png', name: 'Gift'}
        ],
        sections: {
            food: [
                {path: './images/doneristIcon.png', heading: 'Donerist', p: 'Lorem Ipsum is simply dummy text of the printing and '},
                {path: './images/kfcIcon.png', heading: 'KFC', p: 'Lorem Ipsum is simply dummy text of the printing and '},
                {path: './images/kosebasiIcon.png', heading: 'Kosebasi', p: 'Lorem Ipsum is simply dummy text of the printing and '},
                {path: './images/burgerkingIcon.png', heading: 'Burger King', p: 'Lorem Ipsum is simply dummy text of the printing and '},
                {path: './images/kfcIcon.png', heading: 'KFC', p: 'Lorem Ipsum is simply dummy text of the printing and '},
                {path: './images/kosebasiIcon.png', heading: 'Kosebasi', p: 'Lorem Ipsum is simply dummy text of the printing and '},
                {path: './images/burgerkingIcon.png', heading: 'Burger King', p: 'Lorem Ipsum is simply dummy text of the printing and '}/*,
                {path: './images/burgerkingIcon.png', heading: 'Burger King', p: 'Lorem Ipsum is simply dummy text of the printing and '},
                {path: './images/kfcIcon.png', heading: 'KFC', p: 'Lorem Ipsum is simply dummy text of the printing and '},
                {path: './images/kosebasiIcon.png', heading: 'Kosebasi', p: 'Lorem Ipsum is simply dummy text of the printing and '},
                {path: './images/burgerkingIcon.png', heading: 'Burger King', p: 'Lorem Ipsum is simply dummy text of the printing and '}*/
            ],
            medicine: [],
            parfum: [],
            clothes: [],
            gift: []
        },
        active: {
            all:true,
            food: false,
            medicine: false,
            parfum: false,
            clothes: false,
            gift: false
        },
        karuselImages: [],
        designed: false,
        shown: 3, 
        start: 0,
        end: 7
    }
    MainSectionClickHandler = (e) =>{
        console.log(e.target);
        const id = e.target.getAttribute('id');
        const active = this.state.active;
        for(const key in this.state.active){
            if(key === id.toLowerCase()){
                active[key] = true;
            }
            else{
                active[key] = false;
            }
        }
        this.setState({active: active});
        this.KaruselImages();
    }
    SetDesign = () =>{
        if(this.state.karuselImages.length === 1){
            document.getElementById('id0').id = 'id3';
            this.setState({shown:0});
        }
        if(this.state.karuselImages.length === 2){
            document.getElementById('id0').id = 'id2';
            document.getElementById('id1').id = 'id3';
            this.setState({shown:1});
        }
        if(this.state.karuselImages.length === 3){
            document.getElementById('id0').id = 'id2';
            document.getElementById('id1').id = 'id3';
            document.getElementById('id2').id = 'id4';
            this.setState({shown:1});
        }
        if(this.state.karuselImages.length === 4){
            document.getElementById('id0').id = 'id2';
            document.getElementById('id1').id = 'id3';
            document.getElementById('id2').id = 'id4';
            document.getElementById('id3').id = 'id5';
            this.setState({shown:1});
        }
        if(this.state.karuselImages.length === 5){
            document.getElementById('id0').id = 'id1';
            document.getElementById('id1').id = 'id2';
            document.getElementById('id2').id = 'id3';
            document.getElementById('id3').id = 'id4';
            document.getElementById('id4').id = 'id5';
            this.setState({shown:2});
        }
        if(this.state.karuselImages.length === 6){
            document.getElementById('id0').id = 'id1';
            document.getElementById('id1').id = 'id2';
            document.getElementById('id2').id = 'id3';
            document.getElementById('id3').id = 'id4';
            document.getElementById('id4').id = 'id5';
            document.getElementById('id5').id = 'id6';
            this.setState({shown:2});
        }
        
    }
    AllImages = () =>{
        const karuselImages = [];
        for(const section in this.state.sections){
            if(this.state.sections[section].length>0){
                for(let index in this.state.sections[section]){
                    karuselImages.push(this.state.sections[section][index]);
                }
            }
        }
        this.setState({karuselImages: karuselImages});
        this.setState({end: karuselImages.length-1});
        this.setState({designed: true});
    }
    componentDidMount = () =>{
        this.AllImages();
    }
    ThisImage = (section) =>{
        let karuselImages = [];
        karuselImages = this.state.sections[section];
        this.setState({karuselImages: karuselImages});
    }
    KaruselImages = () =>{
        for(const key in this.state.active){
            if(this.state.active[key]){
                if(key === 'all'){
                    this.AllImages();
                }
                else{
                    this.ThisImage(key);
                }
            };
        }
    }
    RightChange = (index) =>{
        var shown = this.state.shown;
        var start = this.state.start;
        var end = this.state.end;
        for(var i = start; i <= end; i++){
            if((i - (index-shown)) <=0){
                document.getElementById('id' + i).id = 'id' + (i - (index-shown));
                document.getElementById('id' + (i - (index-shown))).style = "position: absolute;right: 500%;";

                document.getElementById('r' + i).id = 'r' + (i - (index-shown));
                document.getElementById('r' + (i - (index-shown))).style = "width: 0;";
            }
            else{
                if((i - (index-shown)) >= 7){
                    document.getElementById('id' + i).id = 'id' + (i - (index-shown));
                    document.getElementById('id' + (i - (index-shown))).style = "position: absolute;left: 500%;";
                    if((i - (index-shown))<=6){
                        document.getElementById('r' + i).id = 'r' + (i - (index-shown));
                        document.getElementById('r' + (i - (index-shown))).removeAttribute('style');
                    }
                    else{
                        document.getElementById('r' + i).id = 'r' + (i - (index-shown));
                        document.getElementById('r' + (i - (index-shown))).style = "width:0"
                    }
                }else{
                    document.getElementById('id' + i).id = 'id' + (i - (index-shown));
                    document.getElementById('id' + (i - (index-shown))).removeAttribute('style');
                    if((i - (index-shown))< 0){
                        document.getElementById('r' + i).id = 'r' + (i - (index-shown));
                        document.getElementById('r' + (i - (index-shown))).style = "width: 0";
                        
                    }
                    else{
                        if((i - (index-shown)) < 7){
                            document.getElementById('r' + i).id = 'r' + (i - (index-shown));
                            document.getElementById('r' + (i - (index-shown))).removeAttribute('style');
                        }
                        else{
                            document.getElementById('r' + i).id = 'r' + (i - (index-shown));
                            document.getElementById('r' + (i - (index-shown))).style = "width:0"
                        }
                    }
                }
            }
        }
        this.setState({shown: index});
        this.setState({start: start - (index-shown)});
        this.setState({end: end - (index-shown)});
    }
    LeftChange = (index) =>{
        var shown = this.state.shown;
        var start = this.state.start;
        var end = this.state.end;
        for(var i = end; i >= start; i--){
            if((i + (shown - index)) > 6){
                document.getElementById('id' + i).id = 'id' + (i + (shown - index));
                document.getElementById('id' + (i + (shown - index))).style = "position: absolute; left: 500%;";  
                document.getElementById('r' + i).id = 'r' + (i + (shown - index));
                document.getElementById('r' + (i + (shown - index))).style = "width: 0;";
            }
            else{
                if(i<0){
                    document.getElementById('id' + i).id = 'id' + (i + (shown - index));
                    document.getElementById('id' + (i + (shown - index))).style = "position: absolute;right: 500%;"
                    if((i + (shown - index))>=0){
                        document.getElementById('r' + i).id = 'r' + (i + (shown - index));
                        document.getElementById('r' + (i + (shown - index))).removeAttribute('style');
                    }
                    else{
                        document.getElementById('r' + i).id = 'r' + (i + (shown - index));
                        document.getElementById('r' + (i + (shown - index))).style = "width: 0;";
                    }
                }
                else{
                    document.getElementById('id' + i).id = 'id' + (i + (shown - index));
                    document.getElementById('id' + (i + (shown - index))).removeAttribute('style');
                    if((i + (shown - index))>6){
                        document.getElementById('r' + i).id = 'r' + (i + (shown - index));
                        document.getElementById('r' + (i + (shown - index))).style = "width: 0;";
                    } 
                    else{
                        document.getElementById('r' + i).id = 'r' + (i + (shown - index));
                        document.getElementById('r' + (i + (shown - index))).removeAttribute('style'); 
                    }
                }
            }
        }
        this.setState({shown: index});
        this.setState({start: start + (shown - index)});
        this.setState({end: end + (shown - index)});
    }
    ChangeImage = (index) =>{
        if(index < this.state.shown){
            this.LeftChange(index);
        }
        if(index > this.state.shown){
            this.RightChange(index);
        }
    }
    RoundClickHandler = (i) =>{
        //const imageIndex = e.target.getAttribute('id').slice(1);
        this.ChangeImage(parseInt(i));
    }
    render = () => {
        return (
            <div className = {classes.PartnershipIndustry}>
                <h1 className = {classes.Heading}>Partners Industry</h1>
                <div className = {classes.KaruselSection}>
                    <div className = {classes.MainSections}>
                        {this.state.images.map(image=>{
                            return(
                                <div id = {image.name} className = {classes.MainSection} onClick = {this.MainSectionClickHandler}>
                                    <img id = {image.name} className = {classes.imageBack} src = './images/iconBackground.png' /> 
                                    <img id = {image.name} className = {classes.Icon} src = {image.imagePath} />
                                    <p id = {image.name} className = {this.state.active[image.name.toLowerCase()]? [classes.ImageP, classes.Active].join(' '): classes.ImageP}>{image.name}</p>
                                </div>
                            )
                        })}
                    </div>
                    <div className = {classes.Karusel}>
                        {this.state.karuselImages.map((image,i)=>{
                            return(
                                <div id = {"id"+i} style = {i>6?{position: 'absolute',left: '2000px'}:null} className = {[classes.section,classes.id].join(' ')} >
                                    <img className = {classes.KaruselImage} src= {image.path} />
                                    <h1 className = {classes.KaruselHeading}>{image.heading}</h1>
                                    <p className = {classes.KaruselP}>{image.p}</p>
                                </div>
                            )
                        })}
                    </div>
                    <div className = {classes.ImageDetection}>
                        {this.state.karuselImages.map((image,i)=>{
                            return(
                                <div onClick = {()=>this.RoundClickHandler(i)} id = {'r'+i} className = {this.state.shown === i? classes.RoundSelected: classes.Round} style = {{width: i>6?'0':null}}>
                                    
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }
}
export default PartnershipIndustry;