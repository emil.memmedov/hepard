import classes from './smartphone.module.css';

const Smartphone = () =>{
    return (
        <div className = {classes.Smartphone}>
            <h1 className = {classes.Heading}>Available for your smartphone.</h1>
            <p className = {classes.Paragraph}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
            <button className = {classes.GooglePlayBtn}><img className = {classes.GPIcon} src = "./images/playMarket.png" />Google Play</button>
            <button className = {classes.AppStoreBtn}><img className = {classes.APIcon} src = "./images/apple.png" />App Store</button>
            <div className = {classes.Images}>
                <div className = {classes.BottomDiv}>
                    <img className = {classes.ImageBottom} src = "./images/phoneFront.png" />
                </div>
                <img className = {classes.ImageCenter} src = "./images/phoneFront.png" />
                <img className = {classes.ImageTop}src = "./images/phoneFront.png" />
            </div>
        </div>
    )
}
export default Smartphone;